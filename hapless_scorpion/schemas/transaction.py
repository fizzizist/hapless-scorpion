from datetime import date
from decimal import Decimal
from uuid import UUID

import pandera as pa


class RawTxnDF(pa.DataFrameModel):
    id: UUID
    account_id: UUID
    description: str | None
    amount: Decimal
    txn_type: str
    happened_on: date
    reconciled: bool


class TxnOutputDF(pa.DataFrameModel):
    id: UUID
    description: str | None
    increased: Decimal | None
    decreased: Decimal | None
    happened_on: date
    reconciled: bool
    balance: Decimal
