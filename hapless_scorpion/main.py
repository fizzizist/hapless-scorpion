from tkinter import BOTH, Frame

from pandastable import config

from hapless_scorpion.tables.account import AccountTable


class HaplessScorpion(Frame):
    def __init__(self, parent=None):
        self.parent = parent
        Frame.__init__(self)
        self.main = self.master
        self.main.title("Hapless Scorpion")
        f = Frame(self.main)
        f.pack(fill=BOTH, expand=1)
        self.table = pt = AccountTable(
            f,
            account_id="019064b4-6458-79a9-80a3-963a27f7d727",
            showtoolbar=True,
            showstatusbar=True,
        )
        options = {"floatprecision": 5}
        config.apply_options(options, pt)
        pt.show()


if __name__ == "__main__":
    app = HaplessScorpion()
    app.mainloop()
