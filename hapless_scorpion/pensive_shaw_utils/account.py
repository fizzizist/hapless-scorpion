from decimal import Decimal
from uuid import UUID

import numpy as np
import pandas as pd
import pandera as pa
import requests
import structlog
from hapless_scorpion.config import config
from hapless_scorpion.schemas.transaction import RawTxnDF, TxnOutputDF

logger = structlog.get_logger(__name__)

TXN_PATH = "/api/v0/transactions"
ACC_PATH = "/api/v0/accounts"


def get_raw_txn_df(account_id: UUID) -> pa.typing.DataFrame[RawTxnDF]:
    url = f"{config.pensive_shaw_url}{TXN_PATH}"
    params = {"account-id": str(account_id)}
    logger.debug("request info", url=url, params=params, psu=config.pensive_shaw_url)
    resp = requests.get(url, params=params)
    if resp.status_code == 200:
        return pd.DataFrame(resp.json())

    if resp.status_code == 404:
        raise ValueError(f"No account for ID {account_id}")

    raise RuntimeError(resp.json())


def patch_txn(payload: dict):
    url = f"{config.pensive_shaw_url}{TXN_PATH}"
    resp = requests.patch(url, json=payload)
    if resp.status_code == 200:
        logger.info("txn successfully updated", payload=payload)
        return

    logger.error(
        "txn update failed",
        payload=payload,
        resp=resp.json(),
        status=resp.status_code,
    )
    raise RuntimeError(resp.json())


def get_account(account_id: UUID) -> dict:
    url = f"{config.pensive_shaw_url}{ACC_PATH}/{account_id}"
    resp = requests.get(url)
    if resp.status_code == 200:
        return resp.json()

    if resp.status_code == 404:
        raise ValueError(f"No account for ID {account_id}")

    raise RuntimeError(resp.json())


def get_increased_decreased(
    txn_type: str, amount: Decimal, acc_type: str
) -> tuple[Decimal | None, Decimal | None]:
    if (txn_type == "DEBIT" and acc_type == "ASSET") or (
        txn_type == "CREDIT" and acc_type == "LIABILITY"
    ):
        return (None, amount)
    if (txn_type == "CREDIT" and acc_type == "ASSET") or (
        txn_type == "DEBIT" and acc_type == "LIABILITY"
    ):
        return (amount, None)
    raise RuntimeError(
        f"Invalid transaction type: {txn_type}, or account type: {acc_type}"
    )


def calculate_balances(arr: np.ndarray) -> np.ndarray:
    curr_balance = Decimal("0.0")
    balances = []
    for item in arr:
        if item[0] is not None:
            curr_balance += item[0]
        else:
            curr_balance -= item[1]
        balances.append(curr_balance)
    return np.array(balances)


def get_all_accounts() -> pd.DataFrame:
    url = f"{config.pensive_shaw_url}{ACC_PATH}"
    resp = requests.get(url)
    if resp.status_code == 200:
        return pd.DataFrame(resp.json())

    raise RuntimeError(resp.json())


def get_hierarchical_acc_name(
    acc_graph: dict, name_map: dict, acc_id: UUID, hier_name: str
) -> str:
    if acc_graph[acc_id] is None:
        return f"{name_map[acc_id]}{hier_name}"

    return get_hierarchical_acc_name(
        acc_graph, name_map, acc_graph[acc_id], f":{name_map[acc_id]}{hier_name}"
    )


def get_parent_name_map() -> pd.DataFrame:
    accounts = get_all_accounts()
    account_parent_graph = {
        _id: parent_id for _id, parent_id in accounts[["id", "parent_id"]].to_numpy()
    }
    acc_name_map = {_id: name for _id, name in accounts[["id", "name"]].to_numpy()}
    accounts["transfer_account"] = np.vectorize(get_hierarchical_acc_name)(
        account_parent_graph, acc_name_map, accounts["id"], ""
    )
    return accounts[["id", "transfer_account"]]


def get_txn_df(account_id: UUID, acc_type: str) -> pa.typing.DataFrame[TxnOutputDF]:
    raw_df: pa.typing.DataFrame[RawTxnDF] = get_raw_txn_df(account_id)

    # decimalify the amount
    raw_df.amount = np.vectorize(lambda amount: Decimal(amount))(raw_df.amount)

    # sort DF by date
    raw_df = raw_df.sort_values(by="happened_on")

    # drop account_id
    raw_df.drop(columns="account_id", inplace=True)

    # replace paired_account_id with paired account name
    account_name_map = get_parent_name_map().rename(columns={"id": "paired_account_id"})
    raw_df = raw_df.merge(account_name_map, on="paired_account_id", how="inner").drop(
        columns="paired_account_id"
    )

    # transform txn_type and amount into increased/decreased arrays
    raw_df["increased"], raw_df["decreased"] = np.vectorize(get_increased_decreased)(
        raw_df.txn_type, raw_df.amount, acc_type
    )
    raw_df.drop(columns=["txn_type", "amount"], inplace=True)

    # calculate balances
    raw_df["balance"] = calculate_balances(
        raw_df[["increased", "decreased"]].to_numpy()
    )

    return raw_df
