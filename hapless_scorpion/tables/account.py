from datetime import datetime
from decimal import Decimal
from tkinter import Entry, Frame, Listbox, StringVar, messagebox
from uuid import UUID

import structlog
from hapless_scorpion.pensive_shaw_utils.account import (
    calculate_balances,
    get_account,
    get_parent_name_map,
    get_txn_df,
    patch_txn,
)
from pandastable import Table
from tkcalendar import DateEntry

logger = structlog.get_logger(__name__)


class AccountTable(Table):
    def __init__(self, frame: Frame, account_id: UUID, **kwargs):
        self.acc = get_account(account_id)
        df = get_txn_df(self.acc["id"], self.acc["acc_type"])
        self.paired_accounts_name_map = dict(
            get_parent_name_map()[["transfer_account", "id"]].to_numpy()
        )
        super().__init__(frame, dataframe=df, **kwargs)
        self.addRows(num=1)

    def drawDateEntry(self, row, col):
        date_txt = self.model.getValueAt(row, col)
        set_date = datetime.strptime(date_txt, "%Y-%m-%d").date()
        x1, y1, x2, y2 = self.getCellCoords(row, col)
        w = x2 - x1
        self.cellentryvar = txtvar = StringVar()
        txtvar.set(date_txt)
        self.date_entry = DateEntry(
            self.parentframe,
            yearint=set_date.year,
            monthint=set_date.month,
            dayint=set_date.day,
            date_pattern="y-mm-dd",
            textvariable=txtvar,
        )
        self.date_entry.bind("<Return>", lambda x: self.handleCellEntry(row, col))
        self.entrywin = self.create_window(
            x1,
            y1,
            width=w,
            height=self.rowheight,
            window=self.date_entry,
            anchor="nw",
            tag="entry",
        )

    def handleListboxSelect(self, event):
        self.entry.focus_set()
        sel = self.listbox.get(self.listbox.curselection()[0])
        self.cellentryvar.set(sel)

    def handleEntryListboxDown(self, event):
        self.listbox.focus_set()
        self.listbox.select_set(0)

    def handle_left_click(self, event):
        if hasattr(self, "date_entry"):
            self.date_entry.destroy()
        if hasattr(self, "listbox"):
            self.listbox.destroy()

        super().handle_left_click(event)

    def drawListboxEntry(self, row, col):
        txt = self.model.getValueAt(row, col)
        x1, y1, x2, y2 = self.getCellCoords(row, col)
        w = x2 - x1
        self.cellentryvar = txtvar = StringVar()
        txtvar.set(txt)
        self.entry = Entry(
            self.parentframe,
            textvariable=txtvar,
        )
        self.entry.bind("<Return>", lambda x: self.handleCellEntry(row, col))
        self.entry.bind("<Key>", lambda x: self.handleCellEntryAutocomplete(row, col))
        self.entrywin = self.create_window(
            x1,
            y1,
            width=w,
            height=self.rowheight,
            window=self.entry,
            anchor="nw",
            tag="entry",
        )
        self.listbox = Listbox(self.parentframe)
        # when user presses down in starts selecting from listbox
        self.entry.bind("<Down>", self.handleEntryListboxDown)
        self.listbox.bind("<Return>", self.handleListboxSelect)
        self.update_lb(self.paired_accounts_name_map.keys())
        self.lbwin = self.create_window(
            x1,
            y2,
            width=w,
            window=self.listbox,
            anchor="nw",
            tag="listbox",
        )
        self.entry.focus_set()
        self.entry.select_range(0, "end")

    def update_lb(self, data):
        self.listbox.delete(0, "end")
        for item in data:
            self.listbox.insert("end", item)

    def drawCellEntry(self, row, col, text=None):
        col_name = self.model.getColumnName(col)
        if (col_name in {"balance", "id"}) or not self.editable:
            return

        if col_name == "reconciled":
            # reconciled is a bool, and should just flip when double-clicked
            value = not self.model.getValueAt(row, col)
            self.model.setValueAt(value, row, col)
            self.drawText(row, col, value, align=self.align)
            self.update_txn(row)
            return

        if col_name == "happened_on":
            self.drawDateEntry(row, col)
            return

        if col_name == "transfer_account":
            self.drawListboxEntry(row, col)
            return

        super().drawCellEntry(row, col, text=text)

    def handleCellEntryAutocomplete(self, row, col):
        value = self.cellentryvar.get()
        data = [
            item
            for item in self.paired_accounts_name_map
            if value.lower() in item.lower()
        ]
        self.update_lb(data=data)

    def handleCellEntry(self, row, col):
        value = self.cellentryvar.get()
        logger.debug(
            "cell editted",
            row=row,
            col=col,
            value=value,
            df=self.model.df,
            current_col=self.getSelectedColumn(),
        )
        if self.filtered == 1:
            df = self.dataframe
        else:
            df = None

        col_name = self.model.getColumnName(col)
        if col_name in {"increased", "decreased"}:
            if not value.isnumeric():
                messagebox.showerror(
                    "Invalid Data", "The input value must be a decimal"
                )
                return
            op_col_name = "increased" if col_name == "decreased" else "decreased"
            op_col = self.model.df.columns.get_loc(op_col_name)
            self.model.setValueAt(None, row, op_col, df=df)
            value = Decimal(value)
            self.model.setValueAt(value, row, col, df=df)
            self.recalculate_balances()
        elif col_name == "transfer_account":
            self.delete("listbox")
            self.model.setValueAt(value, row, col, df=df)
        else:
            self.model.setValueAt(value, row, col, df=df)

        self.drawText(row, col, value, align=self.align)
        self.delete("entry")
        self.update_txn(row)
        self.gotonextCell()

    def recalculate_balances(self):
        self.model.df["balance"] = calculate_balances(
            self.model.df[["increased", "decreased"]].to_numpy()
        )

    def _get_txn_type(
        self, increased: Decimal | None, decreased: Decimal | None
    ) -> str:
        logger.debug("getting txn type", increased=increased, decreased=decreased)
        if increased is not None:
            if self.acc["acc_type"] == "LIABILITY":
                return "DEBIT"
            if self.acc["acc_type"] == "ASSET":
                return "CREDIT"

        if decreased is not None:
            if self.acc["acc_type"] == "LIABILITY":
                return "CREDIT"
            if self.acc["acc_type"] == "ASSET":
                return "DEBIT"

        logger.error(
            "attempting to get transaction",
            increased=increased,
            decreased=decreased,
            acc=self.acc,
        )
        raise RuntimeError("unable to get transaction type")

    def update_txn(self, row):
        txn_record = self.model.getRecordAtRow(row).to_dict()
        logger.warning("Paired account ID: NOT IMPLEMENTED YET")
        payload = {
            "id": txn_record["id"],
            "description": txn_record["description"],
            "happened_on": txn_record["happened_on"],
            "amount": str(txn_record["increased"])
            if txn_record["increased"] is not None
            else str(txn_record["decreased"]),
            "txn_type": self._get_txn_type(
                txn_record["increased"], txn_record["decreased"]
            ),
            "reconciled": txn_record["reconciled"],
            "paired_account_id": self.paired_accounts_name_map[
                txn_record["transfer_account"]
            ],
        }
        patch_txn(payload)
